# Kwiff-Bespoke µ-service

Handles event recommendation engine, offering a customer the best possible user experience


TODO
- [] Standard customer profile - will be used for new customers with low activity history (sportId; categoryId)
- [] User profile - will be generated from customer bets and topped up with standard profile if bet amount is low. This profile will contain data for sportId, competitionId, teamId; categoryId, tableId
- [] highlighted_events table in the offering will move to this service


if bespoke is ON
send event:list command to kwiff-bespoke

- get user profile from here
  - get user bets from core-ticket
  - generate profile
  - think about cache
- get events
  - maybe get only relevant info at this point - live and upcoming Sportbooks - sportId, competitionId, teamId; Casino - categoryId, tableId (this might be static)
- loop events and give each a score
- sort and send to customer
-

# to get bespoke (added by klee)
send('bespoke', 'event:bespokelist', {userId: 3, sportId: 0, listId: 'default', startDate: [], listId: 'default'})
send('bespoke', 'event:bespokelist', {userId: 3, sportId: 0, listId: 'default', listId: 'default', startDate: '2018-01-01'})
send('bespoke', 'event:bespokelist', {userId: 3, sportId: 0, listId: 'default', listId: 'default'})
send('bespoke', 'event:list', {userId: 3, sportId: 0, listId: 'default', blocks: {}})


# events:getuserprofile
send('bespoke', 'events:getuserprofile', {userId: 2733, startDate: "2018-09-26T12:50:54.821Z"})


# apache benchmarking to find out servers capacity, performance and any code discreapncies.

- Instructions:
 - Install Apache2 (howto -> google)
 - Command line performance test:
  - Primitive approach can be initialised using 'watch -n0.1'
    - Example:
      - `watch -n 0.1 'time curl -X POST "http://localhost:5000/docs/bespoke/events:getuserprofile" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"userId\": 2777 }"'`
        - Where:
          - `-n time in seconds; min time: 0.1s
  - Apache is a designated tool and for kwiff would be used this way:
    - Example:
      - `ab -n 10000 -c 250 -k -v 1 -T application/json -p request.json "http://localhost:5000/docs/bespoke/events:getuserprofile"`
        - Where:
          - '-p requst.json is file containing json payload', in this case: {"userId": 2777}
