dbsetup:
	knex migrate:latest && knex seed:run

dbrollback:
	knex migrate:rollback

dbreseed:
	knex seed:run
