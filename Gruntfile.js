/*
 * Kwiff
 *
 * Copyright (c) 2016 Kwiff. All rights reserved.
 *
 */

/*eslint-disable no-console, max-len, filenames/match-regex */
const timeGrunt = require('time-grunt')
const matchdep = require('matchdep')

module.exports = grunt => {
  'use strict'

  // Plugin for time metrics in regards to the execution of the tasks.
  timeGrunt(grunt)

  const buildId = grunt.option('BUILD_ID') || '0001'
  console.log(`build_id:${buildId}`)

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      app_name: '<%= pkg.name %>',
      app_version: '<%= pkg.version %>',
      just_target: 'target',
      target: `target/${buildId}`,
      pkg_name: '<%= meta.app_name %>-v<%= meta.app_version %>',
      artifacts_repo_rul: '<%= pkg.publishConfig.registry %>',
      artifacts_repo_name: 'core',
      artifacts_group: 'kwiff.core',
      artifacts_host: 'localhost:8081',
      artifacts_packaging: 'zip',
      artifacts_top_folder: '/nexus/content/repositories/core',
      policy: `\n/*\n* Package: <%= meta.pkg_name %> \n*\n* In case of unauthorized use, reproduction OR references - obtain necessary rights. \n* Copyright &#169; 2016 Kwiff - Eaton Gate Gaming LTD. All rights reserved.\n* //DATE of Deployment:${new Date()} \n*/`
    },
    packageJSON: {
      release: {
        pretty: 2,
        srcDir: './',
        destDir: '<%= meta.target %>/<%= meta.pkg_name %>',
        remove: ['repository', 'devDependencies', 'scripts']
      }
    },
    githooks: {
      precommit: {
        'pre-commit': 'eslint'
      },
      prepush: {
        'pre-push': 'eslint'
      }
    },
    cleanTarget: {
      target: ['<%= meta.just_target %>'],
      cleanzip: ['./<%= meta.target %>/<%= meta.pkg_name %>.zip'],
      cleanFolder: ['<%= meta.target %>/<%= meta.pkg_name %>']
    },
    compress: {
      zip: {
        options: {
          archive: './<%= meta.target %>/<%= meta.pkg_name %>.zip',
          mode: 'zip'
        },
        files: [{
          expand: true,
          cwd: './<%= meta.target %>/<%= meta.pkg_name %>/',
          src: ['**/*', '.env'],
          dest: ''
        }]
      }
    },
    replace: {
      policy: {
        src: ['<%= meta.target %>/<%= meta.pkg_name %>/**/*.js'],
        overwrite: true,
        replacements: [{
          from: /^\/\*[a-z]{1,5}-policy\*\//g,
          to: '<%= meta.policy %>'
        }]
      }
    },
    shell: {
      validateStructure: {
        command: ['cat lib/messages/index.js', 'cat lib/server.js', 'cat package.json', 'cat index.js', 'cat knexfile.js'].join('&&'),

        options: {
          callback(err, stdout, stderr, cb) {
            if (!err) {
              cb()
            } else {
              grunt.warn('the folder structure OR/AND the required files are not correct', 3)
              console.dir(err)
              console.dir(stdout)
              console.dir(stderr)
            }
          },
          execOptions: {
            cwd: __dirname
          }
        }
      },
      initializeTargetFolder: {
        command: ['mkdir -p <%= meta.target %>', 'cd <%= meta.target %>', 'mkdir <%= meta.pkg_name %>', 'cd <%= meta.pkg_name %>', 'mkdir logs'].join('&&'),

        options: {
          callback(err, stdout, stderr, cb) {
            if (!err) {
              cb()
            } else {
              grunt.warn('the target directory and its structure could not be initialized', 3)
              console.dir(err)
              console.dir(stdout)
              console.dir(stderr)
            }
          },
          execOptions: {
            cwd: __dirname
          }
        }
      },
      installDependencies: {
        command: ['cd <%= meta.target %>/<%= meta.pkg_name %>', 'npm install --production', 'cd node_modules && ln -nsf ../lib'].join('&&'),

        options: {
          callback(err, stdout, stderr, cb) {
            if (!err) {
              cb()
            } else {
              grunt.warn('Installing the production dependencies failed', 3)
              console.dir(err)
              console.dir(stdout)
              console.dir(stderr)
            }
          },
          execOptions: {
            cwd: __dirname
          }
        }
      },
      knexOldDirectoryStructure: {
        command: ['cd <%= meta.target %>/<%= meta.pkg_name %>/node_modules', 'mv ./knex/bin ./knex/lib'].join('&&'),

        options: {
          callback(err, stdout, stderr, cb) {
            if (!err) {
              cb()
            } else {
              grunt.warn('Installing the production dependencies failed', 3)
              console.dir(err)
              console.dir(stdout)
              console.dir(stderr)
            }
          },
          execOptions: {
            cwd: __dirname
          }
        }
      },
      reduceDuplications: {
        command: ['cd <%= meta.target %>/<%= meta.pkg_name %>', 'npm ddp'].join('&&'),

        options: {
          callback(err, stdout, stderr, cb) {
            if (!err) {
              cb()
            } else {
              grunt.warn('Reducing node_modules duplications failed', 3)
              console.dir(err)
              console.dir(stdout)
              console.dir(stderr)
            }
          },
          execOptions: {
            cwd: __dirname
          }
        }
      },
      uploadArtifact: {
        command: ['curl -v -F "r=<%= meta.artifacts_repo_name %>" -F "g=<%= meta.artifacts_group %>" -F "a=<%= meta.pkg_name %>" -F "v=<%= meta.app_version %>" -F "file=@./<%= meta.target %>/<%= meta.pkg_name %>.zip" -u $(cat ./passfile) <%= meta.artifacts_repo_rul %>/<%= meta.app_name %>/<%= meta.pkg_name %>.zip', 'echo "done"'].join('&&'),

        options: {
          callback(err, stdout, stderr, cb) {
            if (!err) {
              cb()
            } else {
              grunt.warn('Uploading Artifact to the Artifact Repository failed', 3)
              console.dir(err)
              console.dir(stdout)
              console.dir(stderr)
            }
          },
          execOptions: {
            cwd: __dirname
          }
        }
      },
      generateConfigFile: {
        command: ['echo \'artifact_repo="<%= meta.artifacts_repo_name %>"\nartifact_group="<%= meta.artifacts_group %>"\nartifact_name="<%= meta.pkg_name %>"\nartifact_component="<%= meta.app_name %>"\npackaging="<%= meta.artifacts_packaging %>"\nartifact_version="<%= meta.app_version %>"\nartifact_file_path="@./<%= meta.target %>/<%= meta.pkg_name %>.zip"\nuser="admin:admin"\nartifacts_repository_host="<%= meta.artifacts_host %>"\nartifacts_repository_folder="<%= meta.artifacts_top_folder %>/<%= meta.app_name %>"\' > <%= meta.target %>/<%= meta.pkg_name %>.txt', 'echo "done"'].join('&&'),

        options: {
          callback(err, stdout, stderr, cb) {
            if (!err) {
              cb()
            } else {
              grunt.warn('Generate Config file sfailed', 3)
              console.dir(err)
              console.dir(stdout)
              console.dir(stderr)
            }
          },
          execOptions: {
            cwd: __dirname
          }
        }
      }
    },

    copy: {
      toTarget: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: './lib',
          src: ['**/*'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/lib'
        }, {
          expand: true,
          filter: 'isFile',
          cwd: './migrations',
          src: ['**/*'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/migrations'
        }, {
          expand: true,
          filter: 'isFile',
          cwd: './seeds', //LATER dev OR Prod will be handled accordingly
          src: ['**/*'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/seeds'
        }, {
          expand: true,
          filter: 'isFile',
          cwd: './',
          src: ['.env'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/'
        }, {
          expand: true,
          filter: 'isFile',
          cwd: './',
          src: ['knexfile.js'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/'
        }, {
          expand: true,
          filter: 'isFile',
          cwd: './',
          src: ['index.js'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/'
        }, {
          expand: true,
          filter: 'isFile',
          cwd: './static_data',
          src: ['**/*'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/static_data'
        }, {
          expand: true,
          filter: 'isFile',
          cwd: './',
          src: ['newrelic.js'],
          dest: '<%= meta.target %>/<%= meta.pkg_name %>/'
        }]
      }
    },

    bump: {
      options: {
        files: ['package.json', './lib/documentation/docs.json'],
        updateConfigs: ['pkg'],
        commit: false,
        commitMessage: 'Release v%VERSION%',
        commitFiles: ['./<%= meta.target %>/<%= meta.pkg_name %>.zip'],
        createTag: false,
        tagName: 'v%VERSION%',
        tagMessage: 'Version %VERSION%',
        push: false,
        pushTo: 'origin',
        gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
        globalReplace: false,
        prereleaseName: false,
        regExp: false
      }
    },

    eslint: {
      target: ['lib/**/*.js', 'migrations/*.js', 'seeds/**/*.js', 'index.js']
    },

    mochaTest: {
      all: {
        src: ['test/**/*.js']
      }
    }
  })

  // Load the plugins that provide tasks.
  matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks)
  //Rename the task cleanTarget
  grunt.task.renameTask('clean', 'cleanTarget')
  grunt.task.renameTask('package', 'packageJSON')


  grunt.registerTask('default', 'Shows up some help informatin', () => {
    grunt.log.writeln('\nUsage: grunt [command]'.red)
    grunt.log.writeln('\nEach command belongs to a life cycle. Three life cycles are supported:'.red)
    grunt.log.writeln('\nBuild Life Cycle'.cyan)
    grunt.log.writeln('validate -- Validate the code structure and the needed files are in place'.white)
    grunt.log.writeln('code-quality -- Check code quality'.white)
    grunt.log.writeln('initialize -- Initialize the code structure'.white)
    grunt.log.writeln('package\t -- Package source files into a distributable format'.white)
    grunt.log.writeln('releasePatch\t -- Release the package and increase its Patch version'.white)
    grunt.log.writeln('releaseMinor\t -- Release the package and increase its Minor version'.white)
    grunt.log.writeln('releaseMajor\t -- Release the package and increase its Major version'.white)
    grunt.log.writeln('\nClean Life Cycle'.cyan)
    grunt.log.writeln('clean -- Delete all files and directories generated by the build life cycle'.white)
  })

  // Validate task.
  grunt.registerTask('validate', 'Validate the project is correct and all necessary information is available', [
    'shell:validateStructure'
  ])

  // Code-Quality task.
  grunt.registerTask('code-quality', 'Check code quality', [
    'eslint',
    'mochaTest'
  ])

  // Initialize task.
  grunt.registerTask('initialize', 'Initilize task for <%= pkg.name %> <%= pkg.type %>...', [
    'code-quality',
    'clean',
    'validate',
    'shell:initializeTargetFolder',
    'copy:toTarget',
    'packageJSON:release',
    'replace:policy'
  ])

  // Package task
  grunt.registerTask('package', 'Package task for <%= pkg.name %> <%= pkg.type %>...', [
    'initialize',
    'shell:installDependencies',
    //'shell:knexOldDirectoryStructure', //Support for Knex new directory strucure
    // 'shell:reduceDuplications',
    //'shell:lockDownDependencies',
    'compress:zip',
    'shell:generateConfigFile',
    'cleanTarget:cleanFolder'
  ])

  // Dry increase of patch version task.
  grunt.registerTask('dry-bump-test', 'Dry check if Version increased for <%= pkg.name %> <%= pkg.type %>...', [
    'bump --dry-run'
  ])

  // Increase patch version task.
  grunt.registerTask('IncreaseVersionPatch', 'Patch Version increased for <%= pkg.name %> <%= pkg.type %>...', [
    'bump:patch'
  ])

  // Increase Minor version task.
  grunt.registerTask('IncreaseVersionMinor', 'Minor Version increased for <%= pkg.name %> <%= pkg.type %>...', [
    'bump:minor'
  ])

  // Increase Major version task.
  grunt.registerTask('IncreaseVersionMajor', 'Major Version increased for <%= pkg.name %> <%= pkg.type %>...', [
    'bump:major'
  ])

  // Clean task.
  grunt.registerTask('clean', 'Cleaning task for <%= pkg.name %> <%= pkg.type %>...', [
    'cleanTarget:target'
  ])

  grunt.registerTask('set-precommit-githooks', 'Set githooks for <%= pkg.name %> ...', [
    'githooks:precommit'
  ])

  grunt.registerTask('set-prepush-githooks', 'Set githooks for <%= pkg.name %> ...', [
    'githooks:prepush'
  ])

  grunt.registerTask('uploadArtifact', 'Deploy task for <%= pkg.name %> <%= pkg.type %>...', [
    'package',
    'shell:uploadArtifact'
  ])

  grunt.registerTask('generateConfigFile', 'Deploy task for <%= pkg.name %> <%= pkg.type %>...', [
    'initialize',
    'shell:generateConfigFile'
  ])

  grunt.registerTask('test', 'Running tests...', ['mochaTest'])
}
