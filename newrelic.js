'use strict'

exports.config = {
  app_name: [process.env.SERVICE_NAME],
  license_key: 'd90936b000d0e206f6e5116e3060318a4b0df4fc',
  logging: {
    level: 'all'
  }
}
