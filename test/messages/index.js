/*kwiff-policy*/
/* eslint-disable no-unused-expressions */

'use strict'

// TODO get:docs for code coverage

// TODO this is placeholder
//
// const request = require('superagent')
// const chai = require('chai')
// const expect = chai.expect
// const serviceBaseUrl = 'localhost:8103/reqrep/'
//
// require('dotenv').load({ path: '.env' })
// const knex = require('knex')({
//   client: 'mysql',
//   connection: {
//     timezone: 'UTC',
//     host: process.env.SERVICE_MYSQL_ADDRESS,
//     user: process.env.SERVICE_MYSQL_USER,
//     password: process.env.SERVICE_MYSQL_PASSWORD,
//     database: process.env.SERVICE_MYSQL_DATABASE
//   }
// })
//
//
// const expectReply = (url, done, payload, expectTests) => {
//   request.post(serviceBaseUrl + url).set('Content-Type', 'application/json')
//     .send(payload).end((err, res) => {
//       expect(res).to.exist
//       expect(res.status).to.equal(200)
//       expectTests(res)
//       done()
//     })
// }
//
// const expectError = (url, done, payload) => {
//   request.post(serviceBaseUrl + url).set('Content-Type', 'application/json')
//     .send(payload).end((err, res) => {
//       expect(res).to.exist
//       expect(res.status).to.equal(200)
//       expect(res.body.message).to.exist
//       expect(res.body.success).to.be.false
//       expect(res.body.code).to.be.exist
//       done()
//     })
// }
//
// const channel = `test-random-channel-${Math.random()}`
// const eventId = 43293
// const sportId = 1
// const eventName = 'test name'
// const eventStart = new Date().toISOString()
// const tvAPIEnabled = true
// const changedBy = 1
// let id = 0
//
// describe('Bespoke', () => {
//   describe('twitter:post', () => {
//     const url = 'twitter/post'
//     const status = `random test status - ${Math.random()}`
//     it('should return error without payload', done => expectError(url, done))
//     it('should return success', done => {
//       expectReply(url, done, { status }, res => {
//         if (res.body.success) {
//           expect(res.body.success).to.be.true
//           expect(res.body.status).to.be.equal(status)
//         } else { // can get error if send to early
//           expect(res.status).to.equal(200)
//           expect(res.body.message).to.exist
//           expect(res.body.success).to.be.false
//           expect(res.body.code).to.be.exist
//         }
//       })
//     })
//     it('should return error on duplicate status', done => expectError(url, done, { status }))
//   })
//
//   describe('twitter:gettweet', () => {
//     const url = 'twitter/gettweet'
//     it('should return tweet data', done => {
//       expectReply(url, done, {}, res => {
//         expect(res.body.success).to.be.true
//         expect(res.body.tweet).to.be.a('string')
//         expect(res.body.tweet.length).to.be.above(0)
//       })
//     })
//   })
//
//   // describe('facebook:post', () => {
//   //   const url = 'facebook/post'
//   //   it('should return something', done => {
//   //     expectReply(url, done, {}, res => {
//   //       console.log(res.body)
//   //       expect(res.body.success).to.be.true
//   //       expect(res.body.allowedToLogin).to.be.true
//   //       expect(res.body.allowedToRegister).to.be.true
//   //     })
//   //   })
//   // })
//
//   // describe('instagram:post', () => {
//   //   const url = 'instagram/post'
//   //   it('should return something', done => {
//   //     expectReply(url, done, {}, res => {
//   //       console.log(res.body)
//   //       expect(res.body.success).to.be.true
//   //       expect(res.body.allowedToLogin).to.be.true
//   //       expect(res.body.allowedToRegister).to.be.true
//   //     })
//   //   })
//   // })
//
//   const expectBoostData = boost => {
//     expect(boost.betId).to.be.above(1) //12260,
//     if (boost.eventId) expect(boost.eventId).to.be.above(1) //381,
//     expect(boost.name).to.be.a('string') //'Martin',
//     expect(boost.hisher).to.be.a('string') //'',
//     expect(boost.boost).to.be.least(1) //8,
//     expect(boost.originalOdds).to.be.a('string') //'4/9',
//     expect(boost.boostedOdds).to.be.a('string') //'18/5',
//     expect(boost.outcomeName).to.be.a('string') //'Konjuh, Ana',
//     expect(boost.outcomeId).to.be.above(0) //12,
//     expect(boost.offerName).to.be.a('string') //'Set 1',
//     expect(boost.home).to.be.a('string') //'Rodina, Evgeniya',
//     expect(boost.away).to.be.a('string') //'Konjuh, Ana',
//     expect(boost.match).to.be.a('string') //'Rodina, Evgeniya vs Konjuh, Ana'
//   }
//
//   describe('boost:data', () => {
//     const url = 'boost/data'
//     it('should return correct boost data', done => {
//       expectReply(url, done, { sportId }, res => {
//         expectBoostData(res.body)
//       })
//     })
//   })
//
//   describe('btsport:add', () => {
//     const url = 'btsport/add'
//     it('should return error if some payload is missing', done => expectError(url, done, {}))
//     it('should return error if some payload is missing', done => expectError(url, done, { channel }))
//     it('should return error if some payload is missing', done => expectError(url, done, { eventId }))
//     it('should return error if some payload is missing', done => expectError(url, done, { eventId, channel }))
//     it('should return error if some payload is missing', done => expectError(url, done, { eventId, channel, changedBy }))
//     it('should return success on add', done => expectReply(url, done, {
//       channel, eventId, changedBy, eventStart, eventName
//     }, res => {
//       expect(res.body.success).to.be.true
//       expect(res.body.id).to.be.above(0)
//     }))
//     it('should return success on add almost the same data (update)', done =>
//       expectReply(url, done, {
//         channel,
//         eventId,
//         eventName,
//         changedBy,
//         eventStart,
//         manual: true,
//         active: true,
//         messageData: {
//           name: 'TestName',
//           originalOdds: '1/2',
//           boostedOdds: '1/1',
//           outcomeName: 'Arcenal',
//           outcomeId: 11,
//           sportId,
//           home: 'Arcenal',
//           away: 'Man United'
//         }
//       }, res => {
//         expect(res.body.success).to.be.true
//         expect(res.body.id).to.be.above(0)
//         id = res.body.id
//       }))
//   })
//
//   describe('btsport:update', () => {
//     const url = 'btsport/update'
//     it('should return error if some payload is missing', done => expectError(url, done, {}))
//     it('should return error if some payload is missing', done => expectError(url, done, { id }))
//     it('should return success and id', done => {
//       expectReply(url, done, { id, tvAPIEnabled, changedBy }, res => {
//         expect(res.body.success).to.be.true
//         expect(res.body.id).to.be.above(0)
//       })
//     })
//   })
//
//   describe('btsport:get', () => {
//     const url = 'btsport/get'
//     it('should return correct data', done => {
//       expectReply(url, done, {}, res => {
//         expect(res.body.success).to.be.true
//         res.body.data.forEach(row => {
//           expect(row.id).to.be.above(0)
//           if (row.betId) expect(row.betId).to.be.above(0)
//           expect(row.channel).to.be.a('string')
//           expect(row.eventId).to.be.above(0)
//           expect(row.eventName).to.be.a('string')
//           expect(row.templateId).to.be.above(0)
//           expect(row.manual).to.be.a('boolean')
//           if (row.messageData) expect(row.messageData).to.be.a('object')
//           expect(row.eventStart).to.be.a('string') //TODO check valid data
//           expect(row.active).to.be.a('boolean')
//           expect(row.tvAPIEnabled).to.be.a('boolean')
//           if (row.xml) expect(row.xml).to.be.a('string')
//         })
//       })
//     })
//   })
//
//   describe('btsport:boost', () => {
//     const url = 'btsport/boost'
//     it('should fail without channel', done => expectError(url, done, {}))
//     it('should return boost data with xml string', done => {
//       expectReply(url, done, { channel }, res => {
//         expect(res.body.success).to.be.true
//         const data = res.body.data
//         expectBoostData(data)
//         expect(data.xml).to.be.a('string')
//       })
//     })
//   })
//
//   describe('btsport:getallboostsforevent', () => {
//     const url = 'btsport/getallboostsforevent'
//     it('should fail without eventId', done => expectError(url, done, {}))
//     it('should return all boosts', done => {
//       expectReply(url, done, { eventId }, res => {
//         const data = res.body.data
//         expect(res.body.success).to.be.true
//         data.forEach(boost => expectBoostData(boost))
//       })
//     })
//   })
//
//   describe('btsport:getchannelsforevents', () => {
//     const url = 'btsport/getchannelsforevents'
//     it('should fail without eventIds', done => expectError(url, done, {}))
//     it('should return something', done => {
//       expectReply(url, done, { eventIds: [eventId] }, res => {
//         res.body.forEach(data => {
//           expect(data.id).to.be.above(0)
//           expect(data.channel).to.be.a('string')
//           expect(data.eventId).to.be.equal(eventId)
//         })
//       })
//     })
//   })
//
//   const expectTemplate = tmpl => {
//     expect(tmpl).to.be.an('array')
//     tmpl.forEach(section => {
//       expect(section).to.be.an('object')
//       Object.keys(section).forEach(key => {
//         expect(key).to.be.oneOf(['odds', 'text', 'subTemplate'])
//         if (key === 'subTemplate') {
//           expect(section[key]).to.be.a('boolean')
//         } else {
//           expect(section[key]).to.be.an('array')
//         }
//         if (key !== 'subTemplate') {
//           section[key].forEach(line => {
//             expect(line).to.be.a('string')
//           })
//         }
//       })
//     })
//   }
//
//   describe('template:get', () => {
//     const url = 'template/get'
//     it('should return single template data', done => {
//       expectReply(url, done, {}, res => {
//         const tmpl = res.body
//         expectTemplate(tmpl)
//       })
//     })
//   })
//
//   describe('template:all', () => {
//     const url = 'template/all'
//     it('should return correct templates data', done => {
//       expectReply(url, done, {}, res => {
//         expect(res.body.success).to.be.true
//         const templates = res.body.templates
//         expect(templates).to.be.an('array')
//         templates.forEach(tmplObj => {
//           expect(tmplObj.id).to.be.above(0)
//           expect(tmplObj.target).to.be.a('string')
//           expectTemplate(JSON.parse(tmplObj.template)) // TODO get object insted of string
//         })
//       })
//     })
//   })
//
//   after('Remove all test data for the user', done => {
//     knex('btsport').where({ channel, event_id: eventId }).del()
//       .then(() => knex('btsport_audit').where({ channel, event_id: eventId }).del())
//       .then(() => done())
//   })
// })
