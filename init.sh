#!/bin/bash

GITHOOK_DIR='./.git/hooks'

if [ "$1" == "configure_githooks" ]; then

  echo "#!/bin/bash
  echo \"pre-push githook started\"
  echo \"Working dir\" $(pwd)
  grunt code-quality
  RESULT=\$?
  [ \$RESULT -ne 0 ] && exit 1

  echo \"pre-push githook ended\"" > $GITHOOK_DIR/pre-push
  /bin/chmod +x $GITHOOK_DIR/pre-push

  echo "#!/bin/bash
  echo \"post-merge githook started\"
  echo \"Working dir\" $(pwd)

  npm install && rm ./node_modules/lib &&
  cd ./node_modules && ln -nsf ../lib

  echo \"post-merge githook ended\"" > $GITHOOK_DIR/post-merge
  /bin/chmod +x $GITHOOK_DIR/post-merge
  echo "** pre-push githook [eslint check] and post-merge githook [shrinkwrap] added **"

elif [ -z "$1" ]; then
  pwd
  PS3='Please choose Action: '
  options=("Remove Deps + Install Deps + Shrinkwrap File" "Update Deps + Shrinkwrap File" "Generate Shrinkwrap File" "Quit")
  select opt in "${options[@]}"
  do
    case "$opt" in
      "Remove Deps + Install Deps + Shrinkwrap File")
        npm install rimraf &&
        ./node_modules/rimraf/bin.js npm-shrinkwrap.json node_modules &&
        npm install && rm ./node_modules/lib &&
        grunt shrinkwrap && cd node_modules && ln -nsf ../lib
        break
        ;;
      "Update Deps + Shrinkwrap File")
        npm install && rm ./node_modules/lib &&
        grunt shrinkwrap && cd node_modules && ln -nsf ../lib
        break
        ;;
      "Generate Shrinkwrap File")
        rm ./node_modules/lib &&
        npm shrinkwrap && cd node_modules && ln -nsf ../lib
        break
        ;;
      "Quit")
        echo "No Actions"
        break
        ;;
      *) echo invalid option;;
    esac
  done
else
  echo 'Given argument is invalid'
fi

exit 0
