/*kwiff-policy*/
const { db } = require('lib/db')
const table = 'events'
const columns = [
  'id AS event_id',
  'status',
  'start AS event_start',
  'sport_id',
  'competition_id',
  'home_team_id',
  'away_team_id',
  'competition_name',
  'home_team_name',
  'away_team_name',
  'created_at',
  'updated_at']


const createEvent = async data => db(table).insert(data)


const updateEvent = async data => db(table).update(data).where('id', data.id)


const getEvent = async id => {
  const rows = await db(table).select(columns).where('id', id)
  return rows[0]
}


const getEvents = async data => {
  const { userId, currentTime, eventSearchPeriod } = data
  return db(table)
    .select(columns)
    .where(function() {
      this.where('start', '>=', currentTime).andWhere('start', '<', db.raw(`'${currentTime}' + INTERVAL ${eventSearchPeriod} DAY`))
    })
    .orWhere(function() {
      this.where('start', '<', currentTime).andWhere('status', 1)
    })
    .orWhere(function() {
      this.where('start', '<', currentTime).andWhere('status', 6)
    })
}


module.exports = {
  createEvent,
  updateEvent,
  getEvent,
  getEvents
}
