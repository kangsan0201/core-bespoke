/*kwiff-policy*/
const { db } = require('lib/db')
const table = 'sports_info'
const columns = [
  'id',
  'name',
  'priority',
  'created_at',
  'updated_at'
]


const getSportsPriority = async () => db(table).select(columns).orderBy('priority', 'ASC')


module.exports = {
  getSportsPriority
}


// CREATE TABLE sports_info (
//     id INT(10) UNSIGNED NOT NULL,
//     name VARCHAR(100) NOT NULL,
//     priority INT(10) UNSIGNED NOT NULL,
//     created_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
//     updated_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3) ON UPDATE CURRENT_TIMESTAMP (3),
//     PRIMARY KEY (id)
// )
// insert into sports_info (id, name, priority) values (1, 'Football', 1);
// insert into sports_info (id, name, priority) values (2, 'Tennis', 2);
// insert into sports_info (id, name, priority) values (4, 'Snooker', 4);
// insert into sports_info (id, name, priority) values (5, 'Darts', 5);
// insert into sports_info (id, name, priority) values (6, 'Rugby', 6);
// insert into sports_info (id, name, priority) values (7, 'American Football', 7);
// insert into sports_info (id, name, priority) values (8, 'Ice Hockey', 8);
// insert into sports_info (id, name, priority) values (10, 'Cricket', 10);
// insert into sports_info (id, name, priority) values (11, 'Football', 11);
// insert into sports_info (id, name, priority) values (12, 'Basketball', 12);
// insert into sports_info (id, name, priority) values (20, 'Boxing', 20);
// insert into sports_info (id, name, priority) values (126, 'MMA', 126);
