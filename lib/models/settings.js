/*kwiff-policy*/
const { db } = require('lib/db')
const table = 'settings'
const columns = [
  'id',
  'data',
  'created_at',
  'updated_at'
]

const exampleQuery = `insert into kwiff_ml.settings (id, data) values (1, '{"eventSearchPeriod":7,"teamSearchPeriod":365,"neighborEventSearchPeriod":3,"trendSearchPeriod":3,"activitySearchPeriod":1,"listComposition":{"teamCnt":15,"neighborCnt":5,"trendCnt":5,"activityCnt":5},"teamStatsSelectLimit":1000,"storeItemValidTimeHour":24,"trendEventValidMinute":5}');`
const exampleJsonObject = {
	"eventSearchPeriod": 7,
	"teamSearchPeriod": 365,
	"neighborEventSearchPeriod": 3,
	"trendSearchPeriod": 3,
	"activitySearchPeriod": 1,
	"listComposition": {
		"teamCnt": 15,
		"neighborCnt": 5,
		"trendCnt": 5,
		"activityCnt": 5
	},
	"teamStatsSelectLimit": 1000,
	"storeItemValidTimeHour": 24,
	"trendEventValidMinute": 5
}


const getSettings = async () => {
  const rows = await db(table).select('data').where({ id: 1 })
  return JSON.parse(rows[0].data)
}


const updateSettings = async settingsWithUpdatedProperties => {
  const updated = await db(table).update({ data: settingsWithUpdatedProperties }).where({ id: 1 })
  return updated === 1
}


const addSettings = async settingsWithNewProperties => {
  const updated = await db(table).update({ data: settingsWithNewProperties }).where({ id: 1 })
  return updated === 1
}


module.exports = {
  getSettings,
  updateSettings,
  addSettings
}



// create table settings (
//   id int(10) unsigned NOT NULL,
//   data TEXT NULL,
//   created_at timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
//   updated_at timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
//   PRIMARY KEY (id)
// )
