/*kwiff-policy*/
const { db } = require('lib/db')
const log = require('core-logging').init()
const table = 'store'
const columns = [
  'user_id',
  'competition_stats',
  'team_stats',
  'live_upcoming_ratio',
  'created_at',
  'updated_at'
]
const TEAM_STATS_MAX_LENGTH = 16777215

// CREATE TABLE store (
//     user_id INT(10) UNSIGNED NOT NULL,
//     competition_stats TEXT NULL,
//     team_stats MEDIUMTEXT NULL,
//     live_upcoming_ratio VARCHAR(255) NULL,
//     created_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
//     updated_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3) ON UPDATE CURRENT_TIMESTAMP (3),
//     PRIMARY KEY (user_id)
// )

const isValidItem = (updatedAt, duration) => {
  let hourDiff = (new Date() - new Date(updatedAt)) / 1000 / 60 / 60
  log.debug({ currTime: new Date(), lastUpdateAt: new Date(updatedAt), duration: `${duration} hours`, hourDiff: `${hourDiff} hour elapsed` })
  return hourDiff < duration
}


const read = async data => {
  const { userId, storeItemValidTimeHour } = data
  const rows = await db(table).select(columns).where({ user_id: userId })
  if (rows.length > 0 && isValidItem(rows[0].updated_at, storeItemValidTimeHour)) {
    log.debug('found store item', { competition_stats_len: rows[0].competition_stats.length, team_stats_len: rows[0].team_stats.length })
    const res = {
      userId: rows[0].user_id,
      competitionStats: JSON.parse(rows[0].competition_stats) || [],
      teamStats: JSON.parse(rows[0].team_stats) || [],
      liveUpcomingRatio: JSON.parse(rows[0].live_upcoming_ratio)
    }
    log.debug('found store item', { competition_item_len: res.competitionStats.length, team_stats_item_count: res.teamStats.length, liveUpcomingRatio: res.liveUpcomingRatio })
    return res
  }
  return {}
}


const upsert = async data => {
  let { userId, sportStats, competitionStats, teamStats, liveUpcomingRatio, storeItemValidTimeHour } = data
  if (typeof teamStats === 'string' && teamStats.length >= TEAM_STATS_MAX_LENGTH) { // max length of TEXT data type
    teamStats = undefined
  }
  const rows = await db(table).select(columns).where({ user_id: userId })
  if (rows.length > 0) {
    if (isValidItem(rows[0].updated_at, storeItemValidTimeHour)) {
      return true
    }
    else {
      // console.log('update store data', userId)
      return db(table).update({
        competition_stats: competitionStats,
        team_stats: teamStats,
        live_upcoming_ratio: liveUpcomingRatio,
        updated_at: new Date() // FIXME use NOW(3) or just remove this column
      })
      .where({ user_id: userId })
    }
  }
  else {
    // console.log('insert store data', userId)
    return db(table).insert({
      user_id: userId,
      competition_stats: competitionStats,
      team_stats: teamStats,
      live_upcoming_ratio: liveUpcomingRatio
    })
  }
}


module.exports = {
  upsert,
  read
}
