/*kwiff-policy*/
const { db } = require('lib/db')
const table = 'bets'


const createBet = async data => db(table).insert(data)


const getSports = async data => {
  const { userId, currentTime, teamSearchPeriod } = data
  const sql = `
    SELECT sport_id, COUNT(*) AS bet_count
    FROM bets
    WHERE user_id = ${userId} AND created_at >= DATE_SUB('${currentTime}', INTERVAL ${teamSearchPeriod} DAY)
    GROUP BY sport_id
    ORDER BY bet_count DESC`
  return db.raw(sql).then(r => r[0])
}


const getTeams = async data => {
  const { userId, currentTime, teamSearchPeriod, teamStatsSelectLimit } = data // team_name
  const sql = `
    SELECT
      sport_id AS sid, team_id AS tid, count(distinct event_id) AS ec, count(*) AS bc
    FROM (
      SELECT
        sport_id, home_team_id AS team_id, home_team_name AS team_name, event_id
      FROM bets
      WHERE
        user_id = ${userId} AND home_team_id != 0 AND created_at >= DATE_SUB('${currentTime}', INTERVAL ${teamSearchPeriod} DAY)
      UNION ALL
      SELECT
        sport_id, away_team_id AS team_id, away_team_name AS team_name, event_id
      FROM bets
      WHERE
        user_id = ${userId} AND away_team_id != 0 AND created_at >= DATE_SUB('${currentTime}', INTERVAL ${teamSearchPeriod} DAY)
    ) AS t
    GROUP BY sid, tid
    ORDER BY sid ASC, ec DESC
    LIMIT ${teamStatsSelectLimit}`
  return db.raw(sql).then(r => r[0])
}


const getTrendEvents = async data => {
  const { currentTime, trendSearchPeriod } = data
  const sql = `
    SELECT
      b.event_id, COUNT(*) AS bet_count,
      b.sport_id,
      b.event_start,
      b.competition_id,
      b.home_team_id,
      b.away_team_id,
      b.competition_name,
      b.home_team_name,
      b.away_team_name
    FROM bets as b
    JOIN events as e ON b.event_id = e.id
    WHERE
      (e.start >= '${currentTime}' AND e.start < '${currentTime}' + INTERVAL ${trendSearchPeriod} DAY)
      OR (e.start < '${currentTime}' and e.status = 1)
      OR (e.start < '${currentTime}' and e.status = 6)
    GROUP BY e.id
    ORDER BY bet_count DESC`
  return db.raw(sql).then(r => r[0])
}


const getLiveUpcomingBetRatio = async data => {
  const { userId } = data
  const sql = `
    SELECT
      ROUND(upcoming_bet_count / (live_bet_count + upcoming_bet_count) * 100) AS upcoming_bet_rate
    FROM (
      SELECT
        user_id,
        SUM(CASE WHEN event_start > created_at THEN 1 ELSE 0 END) AS upcoming_bet_count,
        SUM(CASE WHEN event_start <= created_at THEN 1 ELSE 0 END) AS live_bet_count, NOW(3)
    FROM bets
    WHERE user_id = ${userId}) as t`
  return db.raw(sql).then(r => {
    let upcoming_bet_rate = r[0][0].upcoming_bet_rate || 50
    return { upcoming_bet_rate,
             live_bet_rate: 100 - upcoming_bet_rate }
  })
}


const getSportCompetitions = async data => {
  const { userId, currentTime, sportId, competitionSearchPeriod } = data
  const sql = `
    SELECT sport_id AS sid, competition_id AS cid, competition_name AS cname, COUNT(*) AS bc
    FROM bets
    WHERE
      user_id = ${userId}
      AND created_at >= DATE_SUB('${currentTime}', INTERVAL ${competitionSearchPeriod} DAY)
      AND sport_id = ${sportId || 1}
    GROUP BY competition_id
    ORDER BY bc DESC
    LIMIT 100`
  return db.raw(sql).then(r => r[0])
}


const getUserSportsStats = async data => {
  const { userId, userSportsStatsPeriod } = data
  return db('bets AS b')
    .innerJoin('sports_info AS s', 'b.sport_id', 's.id')
    .select('b.sport_id AS id', 's.name AS name', db.raw('count(*) AS bet_count'))
    .where('b.user_id', userId)
    .andWhere(db.raw(`b.created_at >= DATE_SUB(NOW(3), INTERVAL ${userSportsStatsPeriod || 365} DAY)`))
    .andWhere('b.created_at', '<', db.raw('NOW(3)'))
    .groupBy('b.sport_id')
    .orderBy('bet_count', 'DESC')
}


const getUserCompetitionsStats = async data => {
  const { userId, userCompetitionsStatsPeriod } = data
  return db(table)
    .select('sport_id AS sid', 'competition_id AS cid', 'competition_name AS cname', db.raw('count(*) AS bc'))
    .where('user_id', userId)
    .where('sport_id', 1)
    .andWhere(db.raw(`created_at >= DATE_SUB(NOW(3), INTERVAL ${userCompetitionsStatsPeriod || 365} DAY)`))
    .andWhere('created_at', '<', db.raw('NOW(3)'))
    .groupBy(db.raw('sid, cid ORDER BY sid ASC, bc DESC')) // FIXME db.raw
}


module.exports = {
  createBet,
  getSports,
  getTeams,
  getTrendEvents,
  getLiveUpcomingBetRatio,
  getSportCompetitions,
  getUserSportsStats,
  getUserCompetitionsStats
}
