/*kwiff-policy*/
const { db } = require('lib/db')
const table = 'events'


const getNeighborEvents = async data => {
  const { userId, currentTime, neighborEventSearchPeriod } = data
  // console.log('getNeighborEvents', { userId, currentTime, neighborEventSearchPeriod })
  return []
}


module.exports = {
  getNeighborEvents
}
