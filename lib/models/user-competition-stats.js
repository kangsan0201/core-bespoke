/*kwiff-policy*/
const { db } = require('lib/db')
const table = 'user_competition_stats'
const columns = [
  'id',
  'stats',
  'created_at',
  'updated_at'
]


const getUserCompetitionsPriority = async data => {
  const { userId } = data
  const rows = await db(table).select(columns).where('id', userId)
  return rows[0] ? JSON.parse(rows[0].stats) : []
}


const upsertUserCompetitionsStats = async data => {
  const { userId, stats } = data
  const foundStats = await getUserCompetitionsPriority(data)
  if (!foundStats.length) {
    const insertRes = await db(table).insert({ id: userId, stats })
    return true
  }
  const updateRes = await db(table).update({ id: userId, stats }).where('id', userId)
  return updateRes === 1
}


module.exports = {
  getUserCompetitionsPriority,
  upsertUserCompetitionsStats
}


// CREATE TABLE user_competition_stats (
//     id INT(10) UNSIGNED NOT NULL,
//     stats TEXT,
//     created_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
//     updated_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3) ON UPDATE CURRENT_TIMESTAMP (3),
//     PRIMARY KEY (id)
// );
