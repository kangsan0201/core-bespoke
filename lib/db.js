/*kwiff-policy*/
const log = require('core-logging').init()
const times = {}
let count = 0

const dbApiPerformanceLoggingMsec = process.env.DB_API_PERFORMANCE_LOGGING_MSEC || 500
log.info(`[DB_API] PERFORMANCE DB_API_PERFORMANCE_LOGGING_MSEC: ${dbApiPerformanceLoggingMsec}`)

exports.db = require('knex')({
  client: 'mysql',
  connection: {
    timezone: 'UTC',
    host: process.env.SERVICE_MYSQL_ADDRESS,
    user: process.env.SERVICE_MYSQL_USER,
    password: process.env.SERVICE_MYSQL_PASSWORD,
    database: process.env.SERVICE_MYSQL_DATABASE
  }
})
  .on('query', query => {
    if (count > 10000) count = 0
    const uid = query.__knexQueryUid // eslint-disable-line
    times[uid] = {
      position: count,
      method: query.method,
      sql: query.sql,
      startTime: new Date().getTime(),
      finished: false
    }
    count += 1
  })
  .on('query-response', (response, query) => {
    const uid = query.__knexQueryUid // eslint-disable-line
    if (times[uid]) {
      const diff = new Date().getTime() - times[uid].startTime
      if (diff > dbApiPerformanceLoggingMsec) {
        const perfLog = {
          position: times[uid].position,
          method: times[uid].method,
          sql: times[uid].sql,
          diff
        }
        log.info(`[DB_API] PERFORMANCE: ${diff} ms`, perfLog)
      }
      delete times[uid]
    }
  })
