/*kwiff-policy*/

'use strict'

// TODO this is placeholder
const { db } = require('./db')
// const sbLoader = require('core-sb-loader')
// const boot = require('./boot')
const express = require('express')
const { ServiceHub } = require('core-messaging')
const util = require('util')

const getEnvVar = name => {
  if ((typeof process.env[name])[0] === 'u') throw new Error(`Environment variable ${name} not set.`)
  return process.env[name]
}

const config = {
  service: { name: getEnvVar('SERVICE_NAME'), id: getEnvVar('SERVICE_ID') },
  reqrep: { port: parseInt(getEnvVar('REQREP_LISTEN_PORT'), 10) },
  pubsub: {
    redis: { address: getEnvVar('PUBSUB_REDIS_ADDRESS'), port: parseInt(getEnvVar('PUBSUB_REDIS_PORT'), 10) }
  },
  comcon: {
    redis: { address: getEnvVar('COMCON_REDIS_ADDRESS'), port: parseInt(getEnvVar('COMCON_REDIS_PORT'), 10) }
  },
  services: {}
}

if (process.env.PUBSUB_REDIS_PASSWORD) config.pubsub.redis.password = process.env.PUBSUB_REDIS_PASSWORD
if (process.env.COMCON_REDIS_PASSWORD) config.comcon.redis.password = process.env.COMCON_REDIS_PASSWORD

Object.keys(process.env)
  .map((envVar, i, arr) => {
    if (envVar.startsWith('SERVICE_') && envVar.endsWith('_FQDN')) {
      const service = envVar.split('_')[1]
      if (arr.indexOf(`SERVICE_${service}_PORT`) !== -1) return service
    }
    return null
  })
  .filter(service => service !== null)
  .forEach(service => {
    config.services[service.toLowerCase()] = {
      fqdn: process.env[`SERVICE_${service}_FQDN`],
      port: parseInt(process.env[`SERVICE_${service}_PORT`], 10)
    }
  })

// Internal communication
const log = require('core-logging').init()
const app = express()
const hub = new ServiceHub(app, config, log.child({ component: 'messaging' }))
require('lib/messages').handle(hub, app)
require('core-status')(app, config)

module.exports.start = done => hub.connect().then(async () => {
  // await sbLoader.start(hub, db)
  // await boot.syncWithSBMapping()
  if (typeof done === 'function') done() // done(err)
})
module.exports.stop = () => hub.disconnect()

// Global promise unhandled rejection handler. Prevents promise chains without `catch()` blocks from be "swallowed".
process.on('unhandledRejection', (reason, p) => {
  log.warn('Unhandled promise rejection!')
  log.debug(util.inspect(reason))
  log.debug(util.inspect(p))
})
