const sbLoader = require('core-sb-loader')
const settingsModels = require('../models/settings')

const MIN_UNIFIED_ID = 10
const syncWithSBMapping = async () => {
  // sync new unified sports
  const sbSports = sbLoader.listSports()
  if (!sbSports) return
  const { sport } = await settingsModels.settingsGet()
  // now find missing sports from unified
  const unifiedInternalSports = sport.filter(s => s.sportId >= MIN_UNIFIED_ID).map(s => s.sportId)

  // get missing sports
  const missingSports = sbSports.filter(s => !unifiedInternalSports.includes(s.id)).map(s => s.id)
  await settingsModels.createMutlipleEntries(missingSports)
}

module.exports = { syncWithSBMapping }
