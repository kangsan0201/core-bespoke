/*kwiff-policy*/

const joiCallback = (err, validData) => (err ? Promise.reject(err) : Promise.resolve(validData))

const handleError = err => {
  const message = err.isJoi ? err.details.map(e => e.message).join('; ') : err
  return ({ code: `${process.env.SERVICE_NAME}_ERROR`, message, success: false })
}

module.exports = {
  joiCallback,
  handleError
}
