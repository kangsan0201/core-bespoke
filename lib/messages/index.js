/*kwiff-policy*/
const j2s = require('joi-to-swagger')
const errors = require('lib/errors')
const log = require('core-logging').init()
const sports = require('lib/services/sports')
const competitions = require('lib/services/competitions')
const featured = require('lib/services/featured')
const settings = require('lib/services/settings')
const consumer = require('lib/services/consumer')


const collectSchemas = obj => Object.keys(obj).filter(msg => msg !== 'docs:get').map(msg => {
  const schema = { msg, service: process.env.SERVICE_NAME }
  try {
    const joiSchema = obj[msg]()
    if (joiSchema.isJoi) schema.schema = j2s(joiSchema)
  } catch (e) {
    //  ignore error
  }
  return schema
})


const messageFnBind = {
  'bespoke:getsportslist': sports.getList, // send('bespoke', 'bespoke:sports', { userId: 75, currentTime: '2018-01-01' })
  'bespoke:createsportslist': sports.createList, // send('bespoke', 'bespoke:createsportslist', { userId: 75 })

  'bespoke:getcompetitionslist': competitions.getList, // send('bespoke', 'bespoke:getcompetitionslist', { userId: 75 })
  'bespoke:createcompetitionslist': competitions.createList, // send('bespoke', 'bespoke:createcompetitionslist', { userId: 75 })

  'bespoke:featuredlist': featured.getList, // send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 75 })

  'settings:read': settings.read, // send('bespoke', 'settings:read', {})
  'settings:update': settings.update, // send('bespoke', 'settings:update', { trendSearchPeriod: 1 })
  'settings:create': settings.create, // send('bespoke', 'settings:create', { myTestProperty: 100 })
  'settings:remove': settings.remove // send('bespoke', 'settings:remove', { property: 'myTestProperty' })
}


const comconMessageFnBind = {
  'ticket:accepted': consumer.consumeTicketAccept,
  'event:update': consumer.consumeEventUpdate
}


const addSuccess = result => {
  if (typeof result === 'object' && result !== null && result.success !== false) {
    result.success = true
  }
  return result
}


const handle = hub => {
  Object.keys(messageFnBind).forEach(msg => {
    const fn = messageFnBind[msg]
    hub.reqrep.on(msg, data => {
      log.info(`[REQREP ON] ${msg} ${msg === 'events:getuserprofile' ? JSON.stringify(data) : ''}`)
      return fn(hub, data).then(addSuccess).catch(errors.handleError)
    })
    log.info(`[REQREP] ${msg} registered`)
  })
  Object.keys(comconMessageFnBind).forEach(msg => {
    const fn = comconMessageFnBind[msg]
    hub.comcon.on(msg, data => {
      if (msg === 'event:update') log.info(`[COMCON] ${msg.toUpperCase()}`)
      return fn(hub, data).then(addSuccess).catch(errors.catchError)
    })
    log.info(`[COMCON] ${msg} registered`)
  })
}


module.exports = {
  handle
}
