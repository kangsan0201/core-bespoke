/*kwiff-policy*/
const errors = require('lib/errors')
const log = require('core-logging').init()
const { getSettings } = require('lib/models/settings')
const { getUserSportsPriority, upsertUserSportsStats } = require('lib/models/user-sports-stats')
const { getSportsPriority } = require('lib/models/sports-info')
const { getUserSportsStats } = require('lib/models/bets')


const getList = async (hub, data) => {
  const { userId } = data
  const [ stats, defaultPriority ] = await Promise.all([ getUserSportsPriority(data), getSportsPriority() ])
  const statsMap = stats.reduce((acc, cur) => {
    acc[cur.id] = true
    return acc
  }, {})
  defaultPriority.forEach(cur => {
    if (!statsMap[cur.id]) stats.push(cur)
  })
  return stats.map(s => ({id: s.id, name: s.name }))
}


const createList = async (hub, data) => {
  const { userId } = data
  const { userSportsStatsPeriod } = await getSettings()
  const stats = await getUserSportsStats({ userId, userSportsStatsPeriod })
  if (stats.length > 0) {
    const success = upsertUserSportsStats({ userId, stats: JSON.stringify(stats) })
    return { success }
  }
  log.debug(`createList userId ${userId} does not have stats`)
  return {}
}


module.exports = {
  getList,
  createList
}
