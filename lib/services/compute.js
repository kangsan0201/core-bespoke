/*kwiff-policy*/
const { mergeSort } = require('lib/utils/sort')


const addEventMinimumCount = (teams, from, fromeCnt) => {
  const teamEventMap = teams.reduce((acc, cur) => {
    acc[cur.id] = true
    return acc
  }, {})
  for (let i = 0, addedCnt = 0; i < from.length && addedCnt < fromeCnt ; i += 1) {
    if (!acc[from[i].enent_id]) {
      from[i].score = 0
      teams.push(from[i])
      addedCnt += 1
    }
  }
  return teams
}


const addTrendEvents = (teams, trend, teamCnt, trendCnt) => {
  let lackCnt = teamCnt - teams.length > 0 ? teamCnt - teams.length : 0
  let neededCnt = trendCnt + lackCnt
  const teamEventMap = teams.reduce((acc, cur) => {
    acc[cur.id] = true
    return acc
  }, {})
  for (let i = 0, addedCnt = 0 ; i < trend.length && addedCnt < neededCnt ; i += 1) {
    if (!teamEventMap[trend[i].event_id]) {
      trend[i].score = 0
      teams.push(trend[i])
      addedCnt += 1
    }
  }
  return teams
}


const getEventIdsFeaturedList = data => {
  try {
    const {
      currentTime,
      events,
      sportScoreMap,
      sportCompetitionScoreMap,
      sportTeamScoreMap,
      neighborEvents,
      trendEvents,
      activityEvents,
      liveUpcomingRatio: {
        upcoming_bet_rate,
        live_bet_rate
      },
      listComposition: {
        teamCnt, neighborCnt, trendCnt, activityCnt
      }} = data
    // console.log(sportScoreMap, sportCompetitionScoreMap)
    const filteredEvents = events.filter((event, idx) => {
      const { sport_id, competition_id, home_team_id, away_team_id, start } = event
      // console.log({ sport_id, competition_id, home_team_id, away_team_id, start }, sportCompetitionScoreMap[sport_id][competition_id])
      const sportScore = sportScoreMap[sport_id] || 0
      const competitionScore = sportCompetitionScoreMap[sport_id] && sportCompetitionScoreMap[sport_id][competition_id] ? sportCompetitionScoreMap[sport_id][competition_id].score || 0 : 0
      const homeTeamScore = sportTeamScoreMap[sport_id] && sportTeamScoreMap[sport_id][home_team_id] ? sportTeamScoreMap[sport_id][home_team_id].score : 0
      const awayTeamScore = sportTeamScoreMap[sport_id] && sportTeamScoreMap[sport_id][away_team_id] ? sportTeamScoreMap[sport_id][away_team_id].score : 0
      event.score = sportScore + competitionScore + (homeTeamScore + awayTeamScore)
      // event.score *= start > currentTime ? Math.max(upcoming_bet_rate, 1) : Math.max(live_bet_rate, 1)
      event.score = +event.score.toFixed(4)
      event.scoreSource = {
        sport: sportScore,
        comp: competitionScore,
        home: homeTeamScore,
        away: awayTeamScore
      }
      if (event.score > 0) return true
    })
    let scoredEvents = filteredEvents.length > 0 ? mergeSort(filteredEvents, 'score').slice(0, teamCnt) : []
    console.log('team', scoredEvents.length)
    scoredEvents = addEventMinimumCount(scoredEvents, neighborEvents, neighborCnt) // add min event cnt
    console.log('neighbor', scoredEvents.length)
    scoredEvents = addTrendEvents(scoredEvents, trendEvents, teamCnt, trendCnt) // add lacking teams events and min event cnt
    console.log('trend', scoredEvents.length)
    scoredEvents = addEventMinimumCount(scoredEvents, activityEvents, activityCnt) // add min event cnt
    console.log('activity', scoredEvents.length)
    scoredEvents = mergeSort(scoredEvents, 'event_start').reverse()
    scoredEvents.forEach((e, idx) => {
      if (!idx) console.log('----- events -----')
      // console.log(`${e.sid || e.sport_id} ${e.event_start} ${e.score} ${e.competition_name} ${e.home_team_name} ${e.away_team_name} ${e.scoreSource}`)
      if (e.scoreSource) {
        console.log(`${e.sid || e.sport_id} ${e.event_start} ${e.score} ${e.competition_name} ${e.home_team_name} ${e.away_team_name} `
          + `s:${e.scoreSource.sport} c:${e.scoreSource.comp} h:${e.scoreSource.home} a:${e.scoreSource.away}`)
      }
      else {
        console.log(`${e.sid || e.sport_id} ${e.event_start} ${e.score} ${e.competition_name} ${e.home_team_name} ${e.away_team_name}`)
      }
    })
    return scoredEvents//.map(e => e.event_id)
  } catch(err) {
    log.error('getEventIdsFeaturedList err', err)
    return []
  }
}


module.exports = {
  getEventIdsFeaturedList
}
