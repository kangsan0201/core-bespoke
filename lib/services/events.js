/*kwiff-policy*/
const errors = require('lib/errors')
const log = require('core-logging').init()
const eventModel = require('lib/models/events')
const { addItem, getItem, deleteItem } = require('lib/utils/storage')
const STORAGE_KEY = 'events'


const getEvents = async data => {
  const { eventsValidMinute } = data
  const retrieved = getItem({ key: STORAGE_KEY })
  const elapsedTime = retrieved ? new Date() - new Date (retrieved.addedAt) : 0
  if (retrieved && (elapsedTime > 0 && elapsedTime < (1000 * 60 * eventsValidMinute))) {
    log.debug(`item '${STORAGE_KEY}' found in the storage`)
    return retrieved.data
  }
  log.debug(`item '${STORAGE_KEY}' not found in the storage, execute a query`)
  const events = await eventModel.getEvents(data)
  addItem({ key: STORAGE_KEY, data: events })
  return events
}


module.exports = {
  getEvents
}
