/*kwiff-policy*/
const log = require('core-logging').init()
const { createEvent, updateEvent, getEvent } = require('lib/models/events')
const { createBet } = require('lib/models/bets')


const consumeTicketAccept = async (hub, data) => {
  const tennisSportId = 2
  const t = data.ticket
  const rows = t.bets.map(b => ({
    user_id: t.userId,
    ticket_id: t.id,
    bet_count: t.betCount,
    bet_id: b.betId,
    event_id: b.event.id,
    sport_id: b.sportId,
    event_start: b.event.startDate,
    competition_id: b.event.competition.id,
    competition_name: b.event.competition.name,
    offer_id: b.offerId,
    offer_name: b.offerName,
    outcome_id: b.outcomeId,
    outcome_name: b.outcomeName,
    home_team_id: b.sportId !== tennisSportId ? b.event.homeTeam.id : b.event.homeParticipants.id,
    away_team_id: b.sportId !== tennisSportId ? b.event.awayTeam.id : b.event.awayParticipants.id,
    home_team_name: b.sportId !== tennisSportId ? b.event.homeTeam.name : b.event.homeParticipants.name,
    away_team_name: b.sportId !== tennisSportId ? b.event.awayTeam.name : b.event.awayParticipants.name,
    home_team_short_name: b.sportId !== tennisSportId ? b.event.homeTeam.short : b.event.homeParticipants.short,
    away_team_short_name: b.sportId !== tennisSportId ? b.event.awayTeam.short : b.event.awayParticipants.short,
    created_at: t.acceptedAt
  }))

  const results = await Promise.all(rows.map(r => createBet(r)))
  const failCnt = results.filter(r => r[0] <= 0).length
  return failCnt > 0
}


const consumeEventUpdate = async (hub, event) => {
  try {
    const data = {
      id: event.id,
      status: event.status,
      start: event.startDate,
      completed_at: event.completedAt,
      sport_id: event.sportId,
      competition_id: event.competition.id,
      competition_name: event.competition.name,
      home_team_id: event.homeTeam.id,
      away_team_id: event.awayTeam.id,
      home_team_name: event.homeTeam.name,
      away_team_name: event.awayTeam.name
    }
    const meta = await getEvent(data.id)
    if (!meta) {
      const cid = await createEvent(data)
      if (cid > 0) {
        return true
      }
      else {
        log.debug(`create event failed, id=${event.id}`)
        return false
      }
    }
    const object = {}
    if (data.status !== meta.status) object.status = data.status
    if (String(new Date(data.start)) !== String(new Date(`${meta.start}`))) object.start = data.start
    if (data.completed_at !== meta.completed_at) object.completed_at = data.completed_at
    if (data.competition_id !== meta.competition_id) object.competition_id = data.competition_id
    if (data.home_team_id !== meta.home_team_id) object.home_team_id = data.home_team_id
    if (data.away_team_id !== meta.away_team_id) object.away_team_id = data.away_team_id
    if (Object.keys(object).length > 0) {
      object.id = data.id
      log.info(`event meta updated, id=${data.id}`, object)
      await updateEvent(data).then(ucnt => ucnt === 0).catch(() => false)
    }
    return true
  } catch(err) {
    log.error(`create event failed, id=${event.id}`, 'err', err)
    return false
  }
}


module.exports = {
  consumeTicketAccept,
  consumeEventUpdate
}
