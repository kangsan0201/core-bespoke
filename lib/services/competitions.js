/*kwiff-policy*/
const errors = require('lib/errors')
const log = require('core-logging').init()
const { getSettings } = require('lib/models/settings')
const { getUserCompetitionsPriority, upsertUserCompetitionsStats } = require('lib/models/user-competition-stats')
const { getUserCompetitionsStats } = require('lib/models/bets')


const getList = async (hub, data) => {
  try {
    const { userId, sportId } = data
    const stats = await getUserCompetitionsPriority(data)
    return { list: stats.map(s => ({ sportId: s.sid, competitionId: s.cid })) }
  } catch (err) {
    log.error('getList() err', err)
    return { list: [] }
  }
}


const createList = async (hub, data) => {
  try {
    const { userId } = data
    const { userCompetitionsStatsPeriod } = await getSettings()
    const stats = await getUserCompetitionsStats({ userId, userCompetitionsStatsPeriod })
    if (stats.length > 0) {
      const success = upsertUserCompetitionsStats({ userId, stats: JSON.stringify(stats) })
      return { success }
    }
    log.debug(`createList userId ${userId} does not have stats`)
    return {}
  } catch(err) {
    log.error('createList err', err)
    return {}
  }
}


module.exports = {
  getList,
  createList
}
