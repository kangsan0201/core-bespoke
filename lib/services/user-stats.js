/*kwiff-policy*/
const errors = require('lib/errors')
const log = require('core-logging').init()
const { getSports, getTeams, getSportCompetitions, getLiveUpcomingBetRatio } = require('lib/models/bets')
const readStore = require('lib/models/store').read


const minMaxScale = (min, max, cur) => {
  if (!min) {
    return 0
  }
  else if (min === max) {
    return 1
  }
  else {
    return +((cur - min) / (max - min) || 0.01).toFixed(4)
  }
}


const createSportScoreMap = teamStats => {
  if (!teamStats.length) return {}
  const scoreMap = teamStats.reduce((acc, cur) => {
    const { sid } = cur
    acc[sid] = acc[sid] ? acc[sid] + 1 : 1
    return acc
  }, {})
  const maxCnt = Object.values(scoreMap).reduce((a, b) => {
      return Math.max(a, b)
  })
  Object.keys(scoreMap).forEach(key => {
    scoreMap[key] = minMaxScale(1, maxCnt, scoreMap[key])
  })
  return scoreMap
}


const createSportTeamScoreMap = teamStats => {
  if (!teamStats.length) return {}
  let max = 0
  const min = 1
  const scoreMap = teamStats.reduce((acc, cur) => {
    const { sid, tid } = cur
    if (!acc[sid]) {
      acc[sid] = {}
      max = cur.ec
    }
    cur.score = minMaxScale(min, max, cur.ec)
    acc[sid][tid] = cur
    return acc
  }, {})
  return scoreMap
}


const createSportCompetitionScoreMap = competitionStats => {
  if (!competitionStats.length) return {}
  const max = competitionStats[0].bc
  const min = 1
  const scoreMap = competitionStats.reduce((acc, cur) => {
    const { sid, cid, cname, bc } = cur
    const sidCidObj = {
      name: cname,
      score: minMaxScale(min, max, bc)
    }
    if (acc[sid]) {
      acc[sid][cid] = sidCidObj
    }
    else {
      acc[sid] = { [cid]: sidCidObj }
    }
    return acc
  }, {})
  return scoreMap
}


const getUserStats = async data => {
  try {
    const { userId, storeItemValidTimeHour } = data
    let { competitionStats, teamStats, liveUpcomingRatio } = await readStore({ userId, storeItemValidTimeHour })
    if (!competitionStats || !teamStats || !liveUpcomingRatio) {
      [ competitionStats,
        teamStats,
        liveUpcomingRatio
      ] = await Promise.all([
        getSportCompetitions(data),
        getTeams(data),
        getLiveUpcomingBetRatio(data)
      ])
    }
    const sportScoreMap = createSportScoreMap(teamStats)
    const sportTeamScoreMap = createSportTeamScoreMap(teamStats)
    const sportCompetitionScoreMap = createSportCompetitionScoreMap(competitionStats)
    return { sportScoreMap, sportCompetitionScoreMap, sportTeamScoreMap, competitionStats, teamStats, liveUpcomingRatio }
  } catch(err) {
    log.error('getUserStats error', err)
    return {}
  }
}


module.exports = {
  getUserStats
}
