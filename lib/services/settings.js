/*kwiff-policy*/
const { getSettings, updateSettings, addSettings } = require('lib/models/settings')
const log = require('core-logging').init()


const read = async (hub, data) => {
  const settings = await getSettings()
  console.log(settings)
  return { settings }
}


const create = async (hub, data) => {
  if (!Object.keys(data).length) {
    log.debug('no new settings found')
    return {}
  }
  const settings = await getSettings()
  Object.keys(data).forEach(key => {
    settings[key] = data[key]
  })
  const success = await addSettings(JSON.stringify(settings))
  return { success }
}


const update = async (hub, data) => {
  const updated = {}
  const settings = await getSettings()
  Object.keys(data).forEach(key => {
    if (settings[key]) {
      updated[key] = data[key]
      settings[key] = data[key]
    }
  })
  log.debug('settings updated properties:', updated)
  if (Object.keys(updated).length > 0) {
    const success = await updateSettings(JSON.stringify(settings))
    return { success }
  }
  log.debug('no updated settings found')
  return {}
}


const remove = async (hub, data) => {
  const { property } = data
  if (!property) {
    log.debug('invalid property received')
    return {}
  }
  const settings = await getSettings()
  if (settings[property] === undefined) {
    log.debug('no properties found')
    return {}
  }
  delete settings[property]
  const success = await updateSettings(JSON.stringify(settings))
  return { success }
}


module.exports = {
  read,
  update,
  create,
  remove
}
