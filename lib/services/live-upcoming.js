/*kwiff-policy*/
const betsModel = require('lib/models/bets')
const readStore = require('lib/models/store').read


const getLiveUpcomingBetRatio = async data => {
  const { userId, storeItemValidTimeHour } = data
  let { liveUpcomingRatio } = await readStore({ userId, storeItemValidTimeHour })
  if (!liveUpcomingRatio) liveUpcomingRatio = await betsModel.getLiveUpcomingBetRatio(data)
  return liveUpcomingRatio
}


module.exports = {
  getLiveUpcomingBetRatio
}
