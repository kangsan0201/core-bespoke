/*kwiff-policy*/
const errors = require('lib/errors')
const log = require('core-logging').init()
const { getEvents } = require('lib/services/events')
const { getNeighborEvents } = require('lib/models/neighbors')
const { getActivityEvents } = require('lib/models/activities')
const { getSettings } = require('lib/models/settings')
const { getUserStats } = require('lib/services/user-stats')
const { getTrendEvents } = require('lib/services/trend-events')
const { getEventIdsFeaturedList } = require('lib/services/compute')
const upsertStore = require('lib/models/store').upsert


const createSportIdEventIdListMap = list =>
  list.reduce((acc, cur) => {
    if (acc[cur.sport_id]) acc[cur.sport_id].push(cur.event_id)
    else acc[cur.sport_id] = [cur.event_id]
    return acc
  }, {})


const getList = async (hub, data) => {
  try {
    const { userId, currentTime } = data
    const {
      eventSearchPeriod,
      teamSearchPeriod,
      competitionSearchPeriod,
      neighborEventSearchPeriod,
      trendSearchPeriod,
      activitySearchPeriod,
      listComposition,
      teamStatsSelectLimit,
      storeItemValidTimeHour,
      trendEventValidMinute,
      eventsValidMinute
    } = await getSettings()
    const eventsPromise = getEvents({ userId, currentTime, eventSearchPeriod, eventsValidMinute })
    const userStatsPromise = getUserStats({ userId, currentTime, teamSearchPeriod, competitionSearchPeriod, teamStatsSelectLimit, storeItemValidTimeHour })
    const neighborEventsPromise = getNeighborEvents({ userId, currentTime, neighborEventSearchPeriod })
    const trendEventsPromise = getTrendEvents({ currentTime, trendSearchPeriod, trendEventValidMinute })
    const activityEventPromise = getActivityEvents({ userId, currentTime, activitySearchPeriod })
    const [
      events,
      { sportScoreMap, sportCompetitionScoreMap, sportTeamScoreMap, competitionStats, teamStats, liveUpcomingRatio },
      neighborEvents,
      trendEvents,
      activityEvents
    ] = await Promise.all([
      eventsPromise,
      userStatsPromise,
      neighborEventsPromise,
      trendEventsPromise,
      activityEventPromise
    ])

    // update store with user's stats
    upsertStore({
      userId,
      competitionStats: JSON.stringify(competitionStats),
      teamStats: JSON.stringify(teamStats),
      liveUpcomingRatio: JSON.stringify(liveUpcomingRatio),
      storeItemValidTimeHour
    })

    const list = getEventIdsFeaturedList({
      currentTime,
      events,
      sportScoreMap,
      sportCompetitionScoreMap,
      liveUpcomingRatio,
      sportTeamScoreMap,
      neighborEvents,
      trendEvents,
      activityEvents,
      listComposition
    })

    try {
      const sportIdEventIdListMap = createSportIdEventIdListMap(list)
      console.log(sportIdEventIdListMap)
      const resp = await hub.reqrep.send('offering', 'event:getforbespoke', { eventListGroupBySportId: sportIdEventIdListMap })
      return { events: resp.events }
    } catch(err) {
      log.error('event:geteventsbyids err', err)
      return { events: [] }
    }

    return {
      eventIds: list
    }

  } catch(err) {
    error.debug('feature.getList error', err)
    return {
      events: []
    }
  }
}


module.exports = {
  getList
}



/*
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 75 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 174 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 43057 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 66352 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 72887 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 75841 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 104751 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 105220 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 115848 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 136850 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 137461 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 144333 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 151533 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 151613 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 161651 })
send('bespoke', 'bespoke:featuredlist', { currentTime: '2018-01-01', userId: 175769 })
*/
