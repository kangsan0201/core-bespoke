/*kwiff-policy*/
const errors = require('lib/errors')
const log = require('core-logging').init()
const betsModel = require('lib/models/bets')
const { addItem, getItem, deleteItem } = require('lib/utils/storage')
const STORAGE_KEY = 'trend'


const getTrendEvents = async data => {
  const { trendEventValidMinute } = data
  const retrieved = getItem({ key: STORAGE_KEY })
  const elapsedTime = retrieved ? new Date() - new Date (retrieved.addedAt) : 0
  if (retrieved && (elapsedTime > 0 && elapsedTime < (1000 * 60 * trendEventValidMinute))) {
    log.debug(`item '${STORAGE_KEY}' found in the storage`)
    return retrieved.data
  }
  const trendEvents = await betsModel.getTrendEvents(data)
  addItem({ key: STORAGE_KEY, data: trendEvents })
  return trendEvents
}


module.exports = {
  getTrendEvents
}
