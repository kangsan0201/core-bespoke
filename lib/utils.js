/*kwiff-policy*/

// const log = require('core-logging').init()
const CAMEL = 'CAMEL'
const SNAKE = 'SNAKE'

const changeCase = (data, type) => {
  const newLine = {}
  Object.keys(data).forEach(key => {
    if (type === CAMEL) {
      newLine[key.replace(/(_\w)/g, m => m[1].toUpperCase())] = data[key]
    } else if (type === SNAKE) {
      newLine[key.split(/(?=[A-Z])/).join('_').toLowerCase()] = data[key]
    }
  })
  return newLine
}

const toCamelCase = data => (Array.isArray(data) ? data.map(line => changeCase(line, CAMEL)) : changeCase(data, CAMEL))

const toSnakeCase = data => (Array.isArray(data) ? data.map(line => changeCase(line, SNAKE)) : changeCase(data, SNAKE))

const dynamicSort = property => {
  let sortOrder = 1
  let key = property
  if (property[0] === '-') {
    sortOrder = -1
    key = property.substr(1)
  }
  return (a, b) => {
    const direction = (a[key] > b[key]) ? 1 : 0
    return ((a[key] < b[key]) ? -1 : direction) * sortOrder
  }
}

module.exports = {
  toCamelCase,
  toSnakeCase,
  dynamicSort
}
