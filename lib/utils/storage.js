/*kwiff-policy*/
const errors = require('lib/errors')
const log = require('core-logging').init()
let storage = {}


const createStorage = () => {
  storage = new Map()
  log.info('storage created for trend events')
}


const addItem = item => {
  const { key, data } = item
  storage.set(key, { data, addedAt: new Date() })
}


const removeItem = item => {
  const { key } = item
  storage.delete(key)
}


const getItem = item => {
  const { key } = item
  return storage.get(key) || null
}


createStorage()


module.exports = {
  createStorage,
  getItem,
  addItem,
  removeItem
}
