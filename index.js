/*kwiff-policy*/

const envPath = process.argv.slice(2)[0] || '.env'
require('dotenv').load({ path: envPath })

const path = require('path')
global.basedirname = path.resolve(__dirname)

const main = require('./lib/server')

if (require.main === module) {
  main.start()
} else {
  module.exports = main
}
